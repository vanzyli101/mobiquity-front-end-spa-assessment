import { NgModule } from '@angular/core';
import { NgxMobiquityControlsComponent } from './ngx-mobiquity-controls.component';



@NgModule({
  declarations: [
    NgxMobiquityControlsComponent
  ],
  imports: [
  ],
  exports: [
    NgxMobiquityControlsComponent
  ]
})
export class NgxMobiquityControlsModule { }
