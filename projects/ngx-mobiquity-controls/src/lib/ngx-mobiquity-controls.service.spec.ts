import { TestBed } from '@angular/core/testing';

import { NgxMobiquityControlsService } from './ngx-mobiquity-controls.service';

describe('NgxMobiquityControlsService', () => {
  let service: NgxMobiquityControlsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgxMobiquityControlsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
