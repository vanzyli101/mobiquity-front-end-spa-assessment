import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxMobiquityControlsComponent } from './ngx-mobiquity-controls.component';

describe('NgxMobiquityControlsComponent', () => {
  let component: NgxMobiquityControlsComponent;
  let fixture: ComponentFixture<NgxMobiquityControlsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxMobiquityControlsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxMobiquityControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
