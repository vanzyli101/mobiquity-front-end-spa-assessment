/*
 * Public API Surface of ngx-mobiquity-controls
 */

export * from './lib/ngx-mobiquity-controls.service';
export * from './lib/ngx-mobiquity-controls.component';
export * from './lib/ngx-mobiquity-controls.module';
