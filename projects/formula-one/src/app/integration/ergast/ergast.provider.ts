import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map } from "rxjs";
import { IStanding } from "./models/standing.model";

@Injectable({
    providedIn: 'root'
})
export class ErgastProvider {
    private _rootApi = 'https://ergast.com/api/f1';

    constructor(private _http: HttpClient) { }

    public getDriverStandings(startYear: number, endYear: number = -1) {
        const firstRecordedYear = 1950;

        if (startYear < firstRecordedYear) {
            startYear = firstRecordedYear;
        }

        if (endYear === -1) {
            endYear = new Date().getFullYear();
        }

        if (endYear < firstRecordedYear) {
            endYear = firstRecordedYear;
        }

        if (startYear > endYear) {
            const tempYear = startYear;
            startYear = endYear;
            endYear = tempYear;
        }

        const limit = (endYear - startYear) + 1;
        const offset = startYear - firstRecordedYear;

        const apiCall = this._http
            .get<{ MRData: { StandingsTable: { StandingsLists: IStanding[] } } }>(`${this._rootApi}/driverStandings/1.json?limit=${limit}&offset=${offset}`)
            .pipe(map((resp) => resp.MRData.StandingsTable.StandingsLists));

        return apiCall;
    }
}