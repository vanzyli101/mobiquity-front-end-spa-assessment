import { IDriverStanding } from "./driver-standing.model";

export interface IStanding {
    season: string;
    round: string;
    DriverStandings: IDriverStanding[];
}