import { IConstructor } from "./constructor.model";
import { IDriver } from "./driver.model";

export interface IDriverStanding {
    position: string;
    positionText: string;
    points: string;
    wins: string;
    Driver: IDriver;
    Constructors: IConstructor[];
}