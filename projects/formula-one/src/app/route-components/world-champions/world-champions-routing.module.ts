import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorldChampionsComponent } from './world-champions.component';

const routes: Routes = [{ path: '', component: WorldChampionsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorldChampionsRoutingModule { }
