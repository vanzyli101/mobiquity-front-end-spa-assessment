import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorldChampionsRoutingModule } from './world-champions-routing.module';
import { WorldChampionsComponent } from './world-champions.component';


@NgModule({
  declarations: [
    WorldChampionsComponent
  ],
  imports: [
    CommonModule,
    WorldChampionsRoutingModule
  ]
})
export class WorldChampionsModule { }
