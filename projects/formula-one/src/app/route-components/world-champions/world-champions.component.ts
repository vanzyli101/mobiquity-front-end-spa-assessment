import { Component, OnInit } from '@angular/core';
import { ErgastProvider } from '../../integration/ergast/ergast.provider';
import { IStanding } from '../../integration/ergast/models/standing.model';

@Component({
  selector: 'app-world-champions',
  templateUrl: './world-champions.component.html',
  styleUrls: ['./world-champions.component.scss']
})
export class WorldChampionsComponent implements OnInit {
  public standings: IStanding[] = [];

  constructor(private _ergastProvider: ErgastProvider) { }

  ngOnInit(): void {
    this._ergastProvider.getDriverStandings(2005)
      .subscribe((resp) => this.standings = resp);
  }
}
